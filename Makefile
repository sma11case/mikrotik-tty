#
# Copyright (C) 2009 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk

PKG_VENDOR:=sma11case
PKG_NAME:=rostty
PKG_VERSION:=1.0.0
PKG_RELEASE:=1
PKG_BASE_NAME:=rostty
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_BASE_NAME)-$(PKG_VERSION)



include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/cmake.mk

define Package/$(PKG_NAME)
  SECTION:=base
  CATEGORY:=sma11case
  TITLE:= Mikrotik RouterOS TTY
  URL:=https://gitlab.com/sma11case/rostty
  MAINTAINER:=sma11case
  #DEPENDS:=glib2 libcapi libubox
endef

define Package/$(PKG_NAME)/description
  Mikrotik RouterOS TTY Shell for OpenWrt
endef


define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)
endef


define Build/Clean
	rm -rf $(PKG_BUILD_DIR)
endef

define Package/$(PKG_NAME)/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/rostty $(1)/bin/$(PKG_NAME)
endef

$(eval $(call BuildPackage,$(PKG_NAME)))
