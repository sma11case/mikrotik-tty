## Mikroti tty shell
### a simple tty for mikrotik, support OpenWrt.

### Usage:
```
mikrotik-tty [-f<commands_file>] [-u<username>] [-p<password>] [-P<portNum>] [--quiet] <ip_address>

-u<username> the username to login as.  Default is admin
-p<password> the password to use for login.  Default is empty string
-f<commands_file> the commands send to api.  Default is not use(from stdid only)
-P<port> TCP port to use for API connection.  Default is 8728.
--quiet Suppress all non-API output.  Default is interactive mode.
<ip_address> IP address to connect to.  REQUIRED
```

### Description:
* Source from [Mikrotik Wiki](https://wiki.mikrotik.com/wiki/API_in_C#Sample_Client_.28mikrotik-tty.c.29).
* Add commands file support.
* Add openwrt support.

### Sample Commands File:
**Must Keep Empty Lines! It Means Commit Commands to ROS**

```
/ip/address/print
?interface=pppoe-out1

/ip/address/print
?interface=pppoe-out2

/ip/address/print
?interface=pppoe-out3

/ip/address/print
?interface=pppoe-out4


```

### License:
* Same as [Mikrotik Wiki](https://wiki.mikrotik.com/wiki/API_in_C#Sample_Client_.28mikrotik-tty.c.29).