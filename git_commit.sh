#!/bin/bash

cd "$(dirname "$0")"
SCRIPT_DIR=`pwd`
echo "WorkDir: ${SCRIPT_DIR}"

git add -A 'src/*'
git add -Af 'ROSTTY.xcodeproj/xcshareddata/*'

TODAY=`date '+%Y-%m-%d %H:%M:%S'`
if [ -n "$1" ]; then
	TODAY="$1"
fi
git commit -am "${TODAY}"

git push origin --all
git push origin --tags

exit 0
